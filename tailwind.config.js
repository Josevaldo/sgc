/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      backgroundImage: {
        "gradient-login": " linear-gradient(39.96deg, #F1F1F1 0%, #EEF1FF 100%)",
        "gradient-aside": "linear-gradient(180deg, #4B9EFF 0%, #247ADF 100%)",
        "main": "url('/main.png')"
      },
      
    },

  },
  plugins: [],
};
