'use client'
import { useRouter } from "next/navigation";
import Aside from "@/components/aside";
import { Avatar } from 'primereact/avatar';

const FrontLayout = ({ children }) => {
 const router = useRouter();
  return (
    <div className="w-full flex">
        <div className="w-[18%] bg-[#5665E3] h-auto flex flex-col justify-between items-center py-5 text-white">
        <h1 className="text-3xl font-bold cursor-pointer" onClick={()=>router.push("/front/")}>Gescom</h1>
        <div className="flex flex-col gap-5">
            <p className="cursor-pointer" onClick={()=>router.push("/front/venda/")}>Venda</p>
            <p className="cursor-pointer" onClick={()=>router.push("/front/pos/")}>POS</p>
            <p className="cursor-pointer" onClick={()=>router.push("/front/recebimento/")}>Recebimento</p>
        </div>
        <p className="cursor-pointer">Admnistração</p>
        </div>
        <div className="w-full flex flex-col">
            <div className="w-full h-[15vh] bg-[#DAEBFF] flex justify-end items-center gap-5 px-8 ">
                <div className="flex flex-col">
                    <h3 className="font-bold cursor-pointer">Celestino Adão Zuri</h3>
                    <p className="text-end cursor-pointer">Caixa</p>
                </div>
                <Avatar label="CZ" size="xlarge" shape="circle" className="bg-[#4856D1] cursor-pointer text-white" />
            </div>
            <div className="p-5">
            { children }
            </div>
         
        </div>    
    </div>
  );
}

export default FrontLayout;
