"use client";
import React, { useState } from "react";
import { BreadCrumb } from "primereact/breadcrumb";
import { InputText } from "primereact/inputtext";
import { InputNumber } from "primereact/inputnumber";
import { Calendar } from "primereact/calendar";
import { Dropdown } from "primereact/dropdown";
import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";

export default function POS() {
    const [selectedCity, setSelectedCity] = useState(null);
    const cities = [
        { name: "New York", code: "NY" },
        { name: "Rome", code: "RM" },
        { name: "London", code: "LDN" },
        { name: "Istanbul", code: "IST" },
        { name: "Paris", code: "PRS" },
      ];
  return (
    <div className="w-full">
      <div className="border p-5 rounded-md">
      <div className="flex flex-wrap gap-5">
          <div className="flex flex-col gap-2 flex-1">
            <label htmlFor="product">Produto</label>
            <InputText
              id="product"
              className="border p-2"
              type="text"
              placeholder="Resma de Papel"
            />
          </div>

          <div className="flex flex-col gap-2 max-w-20 mr-3 flex-1">
            <label htmlFor="product">Quantidade</label>
            <InputText
              id="product"
              className="border p-2"
              type="number"
              placeholder="10"
            />
          </div>

          <div className="flex flex-col gap-2 flex-1">
            <label htmlFor="price">Modo de pagamento</label>
            <Dropdown
              value={selectedCity}
              onChange={(e) => setSelectedCity(e.value)}
              options={cities}
              optionLabel="name"
              placeholder="Modo de pagamento"
              className="w-full md:w-14rem border"
            />
          </div>
          <div className="flex flex-col gap-2 mr-3 flex-1">
            <label htmlFor="product">Valor entregue</label>
            <InputText
              id="product"
              className="border p-2"
              type="number"
              placeholder="100.00kz"
            />
          </div>
        </div>

        <div className="flex flex-wrap gap-5 mt-5">
        <div className="flex flex-col gap-2 flex-1">
            <label htmlFor="price">Preço de venda</label>
            <div className="p-inputgroup flex-1">
              <InputNumber placeholder="250.00" className="border p-2" />
              <span className="p-inputgroup-addon">kz</span>
            </div>
          </div>
          <div className="flex flex-col gap-2 flex-1">
            <label htmlFor="price">Tipo de documento</label>
            <Dropdown
              value={selectedCity}
              onChange={(e) => setSelectedCity(e.value)}
              options={cities}
              optionLabel="name"
              placeholder="Factura, factura proforma e factura recibo"
              className="w-full md:w-14rem border"
            />
          </div>
          <div className="flex flex-col gap-2 flex-1">
            <label htmlFor="price">Modo de pagamento</label>
            <Dropdown
              value={selectedCity}
              onChange={(e) => setSelectedCity(e.value)}
              options={cities}
              optionLabel="name"
              placeholder="Modo de pagamento"
              className="w-full md:w-14rem border"
            />
          </div>
        </div>
      </div>
    </div>
  );
}
