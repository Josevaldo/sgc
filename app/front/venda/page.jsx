"use client";
import React, { useState } from "react";
import { BreadCrumb } from "primereact/breadcrumb";
import { InputText } from "primereact/inputtext";
import { InputNumber } from "primereact/inputnumber";
import { Calendar } from "primereact/calendar";
import { Dropdown } from "primereact/dropdown";
import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";

export default function Venda() {
  const items = [{ label: "Venda" }];
  const home = { icon: "pi pi-home", url: "/front" };
  const [date, setDate] = useState(null);
  const [selectedCity, setSelectedCity] = useState(null);
  const [products, setProducts] = useState([]);

  const cities = [
    { name: "New York", code: "NY" },
    { name: "Rome", code: "RM" },
    { name: "London", code: "LDN" },
    { name: "Istanbul", code: "IST" },
    { name: "Paris", code: "PRS" },
  ];
  return (
    <div className="w-full">
      
      <div className="border p-5 rounded-md">
        <div className="flex flex-wrap gap-5">
          <div className="flex flex-col gap-2 flex-1">
            <label htmlFor="username">Cliente</label>
            <InputText
              id="username"
              className="border p-2"
              type="text"
              placeholder="Manuel Neto LDA"
            />
          </div>

          <div className="flex flex-col gap-2 flex-1">
            <label htmlFor="product">Produto</label>
            <InputText
              id="product"
              className="border p-2"
              type="text"
              placeholder="Resma de Papel"
            />
          </div>

          <div className="flex flex-col gap-2 max-w-20 mr-3 flex-1">
            <label htmlFor="product">Quantidade</label>
            <InputText
              id="product"
              className="border p-2"
              type="number"
              placeholder="10"
            />
          </div>

          <div className="flex flex-col gap-2 flex-1">
            <label htmlFor="product">Descrição</label>
            <InputText
              id="product"
              className="border p-2"
              type="text"
              placeholder="Resma de Papel do tipo XPTO"
            />
          </div>

          <div className="flex flex-col flex-1 gap-2">
            <label htmlFor="buttondisplay" className=" block">
              Data de Pagamento
            </label>
            <Calendar
              id="buttondisplay"
              className="border p-2 rounded-md"
              value={date}
              onChange={(e) => setDate(e.value)}
              showIcon
            />
          </div>
        </div>

        <div className="flex flex-wrap gap-5 mt-5">
          <div className="flex flex-col gap-2 flex-1">
            <label htmlFor="price">Preço de venda</label>
            <div className="p-inputgroup flex-1">
              <InputNumber placeholder="250.00" className="border p-2" />
              <span className="p-inputgroup-addon">kz</span>
            </div>
          </div>
          <div className="flex flex-col gap-2 flex-1">
            <label htmlFor="price">Tipo de documento</label>
            <Dropdown
              value={selectedCity}
              onChange={(e) => setSelectedCity(e.value)}
              options={cities}
              optionLabel="name"
              placeholder="Factura, factura proforma e factura recibo"
              className="w-full md:w-14rem border"
            />
          </div>
          <div className="flex flex-col gap-2 flex-1">
            <label htmlFor="price">Modo de pagamento</label>
            <Dropdown
              value={selectedCity}
              onChange={(e) => setSelectedCity(e.value)}
              options={cities}
              optionLabel="name"
              placeholder="Modo de pagamento"
              className="w-full md:w-14rem border"
            />
          </div>
        </div>

        <div className="flex flex-wrap gap-5 mt-5">
            <div className="flex flex-col gap-3 w-full max-w-96">
            <label htmlFor="price">Informação</label>
            <table className="border  rounded-md">
            <thead>
  <tr>
    <th className="border">IVA</th>
    <th className="border">Quantidade em estoque</th>
    <th className="border">Preço</th>
  </tr>
  </thead>
  <tbody>
  <tr>
    <td className="border ps-5">...</td>
    <td className="border ps-5">...</td>
    <td className="border ps-5">...</td>
  </tr>
  
  </tbody>
</table>
            </div>
       
            <div className="flex flex-col gap-3 w-full flex-1">
            <label htmlFor="price">Lista de Produtos</label>
            <table className="border  rounded-md">
            <thead>
  <tr>
    <th className="border">Produto</th>
    <th className="border">Quantidade</th>
    <th className="border">Preço</th>
    <th className="border">Desconto</th>
    <th className="border">Taxa</th>
  </tr>
  </thead>
  <tbody>
  <tr>
    <td className="border ps-5">Resma de Papel do tipo XPTO</td>
    <td className="border ps-5">10</td>
    <td className="border ps-5">250.100.00</td>
    <td className="border ps-5">Resma de Papel</td>
    <td className="border ps-5">sadfghj456</td>
  </tr>
  
  </tbody>
</table>
<div className="bg-blue-500 p-5 w-full mt-[-12px] rounded-b-md">
<span className="text-white font-medium">Total : 257.457.14</span>
</div>
<div className="flex gap-3 w-full">
<button type="button" class="block w-full rounded-md bg-[#233A54] p-5 text-center text-md font-semibold text-white shadow-sm hover:opacity-90 duration-200 ">Registrar</button>
<button type="button" class="block w-full rounded-md bg-[#1A5597] p-5 text-center text-md font-semibold text-white shadow-sm hover:opacity-90 duration-200 ">Sair</button>
</div>

            </div>
        </div>

      </div>
    </div>
  );
}
