"use client"
import { useRouter } from "next/navigation";
export default function Home_() {
  const router = useRouter();
  return (
    <>
    <div className="w-full h-[50vh] bg-main bg-no-repeat bg-left-top bg-cover rounded-sm flex items-start justify-center flex-col ps-10 text-white">
      <h2 className="text-4xl font-medium max-w-2xl">
        Olá <span className="text-sky-500">Celestina</span>, <br/> Seja Bem-Vindo ao Gescom O seu Sistema de Gestão
        Comercial
      </h2>
      
    </div>
    <div className="w-full h-[30vh] bg-white flex items-center justify-between gap-5">
      <div onClick={()=>router.push("/front/venda/")} className="flex-1 bg-[#E5E5E5] shadow-md shadow-[#5665E3] border px-10 py-14 rounded-md flex items-center justify-center cursor-pointer duration-300 hover:bg-[#5665E3] hover:text-white hover:scale-95">
        <p className="font-semibold text-2xl">Venda</p>
      </div>

      <div onClick={()=>router.push("/front/pos/")} className="flex-1 bg-[#E5E5E5] shadow-md shadow-[#5665E3] border px-10 py-14 rounded-md flex items-center justify-center cursor-pointer duration-300 hover:bg-[#5665E3] hover:text-white hover:scale-95">
        <p className="font-semibold text-2xl">Orçamento</p>
      </div>

      <div onClick={()=>router.push("/front/recebimento/")} className="flex-1 bg-[#E5E5E5] shadow-md shadow-[#5665E3] border px-10 py-14 rounded-md flex items-center justify-center cursor-pointer duration-300 hover:bg-[#5665E3] hover:text-white hover:scale-95">
        <p className="font-semibold text-2xl">Recebimento</p>
      </div>
    </div>
    </>
  );
}
