"use client";
import { InputText } from "primereact/inputtext";
import { useRouter } from "next/navigation";
export default function Login() {
  const router = useRouter();
  return (
    <div className="bg-white flex w-full max-w-4xl">
      <div className="bg-white w-full flex-1 py-10 px-7 flex flex-col gap-3">
      <h2 className="text-start text-3xl font-bold mb-5 text-[#505EDB]">Gescom</h2>
        <h2 className="text-start text-2xl font-bold mb-5 text-[#11489A]">Iniciar Sessão</h2>
        <label className="text-start font-medium" htmlFor="email">
          E-mail
        </label>
        <input
          type="email"
          name="email"
          id="email"
          placeholder="Insira o seu email"
          className="border p-2 rounded-sm"
        />

        <label className="text-start font-medium" htmlFor="password">
          Senha
        </label>
        <input
          type="password"
          name="password"
          id="password"
          placeholder="***"
          className="border p-2 rounded-sm"
        />

        <button
          className="w-full p-3 bg-[#659BED] text-white mt-8 hover:opacity-80 duration-200"
          onClick={() => router.push("/front/")}
        >
          Iniciar Sessão
        </button>
      </div>
      <div className="bg-[#4E5ACA] w-full flex-1 hidden sm:block md:flex items-center justify-center px-10">
        <div className="py-8 px-5 bg-[#6876EE] rounded-md text-white">
          <h3 className="mb-5">Lorem ipsum dolor sit amet</h3>
          <p>
            Consectetur adipiscing elit. Nulla gravida mi sem, vulputate mollis
            sapien euismod. Nam ultricies orci in blandit iaculis. Aliquam
            vehicula purus at urna viverra congue. Curabitur in sagittis nunc.
          </p>
        </div>
      </div>
    </div>
  );
}
