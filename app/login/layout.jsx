'use client'

const LoginLayout = ({ children }) => {
 
  return (
    <div className="bg-[#C0D9FF] w-full h-screen flex items-center justify-center">
            { children }
    </div>
  );
}

export default LoginLayout;
